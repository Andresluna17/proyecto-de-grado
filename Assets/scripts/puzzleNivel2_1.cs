﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puzzleNivel2_1 : MonoBehaviour
{
    private GameObject manager,posiciones;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindWithTag("Manager");
        posiciones = GameObject.Find("posiciones");
    }

    // Update is called once per frame
    void Update()
    {
        manager.GetComponent<GameManager>().ordenNivel21(posiciones,gameObject);
    }
}
