﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puzzleNivel3_5 : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject manager,posiciones,entregada;
    public GameObject entregadas;
    void Start()
    {
        manager = GameObject.FindWithTag("Manager");
        posiciones = GameObject.Find("posicion");
        Debug.Log(manager.GetComponent<GameManager>().nivel3.ToString());
        entregada = entregadas.transform.Find(manager.GetComponent<GameManager>().nivel3.ToString()).gameObject;
        entregada.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
        manager.GetComponent<GameManager>().ordenNivel35(posiciones,gameObject);
    }
}
