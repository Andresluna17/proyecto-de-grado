﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public int limitedeintentos=1,score=0,contador=0,nivel3=0;
    private bool gano=false,perdio=false,GameOver=false,tercernivel=false;
    public float tiempo=131f;
    AudioSource audio;
    public AudioClip ganar,perder;

    void Start(){
        audio = GetComponent<AudioSource>();
    }
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
            DontDestroyOnLoad(gameObject);
    }
    void Update()
    {   
        if(tercernivel){
        tiempo-= Time.deltaTime;
        if(tiempo<=0){
            this.cargar("TimeOut");
            StartCoroutine(cargarconespera(4.3f, "mensaje final"));
            StartCoroutine(cargarconespera(8.3f, "puzzle nivel 3.5"));
            tercernivel=false;
        }
        }
        if (limitedeintentos<=0 && GameOver) 
        {
            Debug.Log("fin del juego");
            SceneManager.LoadScene("final");
            GameOver=false;
        }
        if (limitedeintentos == 1)
        {
            GameOver = true;
        }

    }
    public void cargar(string escena)
    {
        SceneManager.LoadScene(escena);
    }
    public void ordenNivel1(GameObject posiciones,GameObject piezas)
    {
			if (piezas.transform.GetChild(0).GetComponent<drap>().ubicado && piezas.transform.GetChild(1).GetComponent<drap>().ubicado && piezas.transform.GetChild(2).GetComponent<drap>().ubicado)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="u/e" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="u/p" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="100")
                {
                    if(!gano){
                        gano = true;
                        score += 100;
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        limitedeintentos = 3;
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 1.1"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    this.fallo();
                    if (limitedeintentos<=0) {
                        this.cargar("final");
                    }
                    else
                    {
                        
                        StartCoroutine(cargarconespera(0.5f, "fallo"));
                        StartCoroutine(cargarconespera(4.3f, "puzzle nivel 1"));
                    }
                }
            }
			}
	}

    public void ordenNivel11(GameObject posiciones,GameObject piezas)
    {
			if (piezas.transform.GetChild(0).GetComponent<drap>().ubicado || piezas.transform.GetChild(1).GetComponent<drap>().ubicado || piezas.transform.GetChild(2).GetComponent<drap>().ubicado)
			{
				if(posiciones.transform.GetChild(0).GetComponent<Detector>().pieza.name=="60")
                {
                    if(!gano){
                        gano = true;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        limitedeintentos = 3;
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 1.2"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;

                    this.fallo();
                    if (limitedeintentos <= 0)
                    {
                        this.cargar("final");
                    }
                    else
                    {
                        StartCoroutine(cargarconespera(0.5f, "fallo"));
                        StartCoroutine(cargarconespera(4.3f, "puzzle nivel 1.1"));
                    }
                }
            }
			}
	}
    public void ordenNivel12(GameObject posiciones,GameObject piezas)
    {
			if (piezas.transform.GetChild(0).GetComponent<drap>().ubicado || piezas.transform.GetChild(1).GetComponent<drap>().ubicado || piezas.transform.GetChild(2).GetComponent<drap>().ubicado)
			{
				if(posiciones.transform.GetChild(0).GetComponent<Detector>().pieza.name=="83")
                {
                    if(!gano){
                        gano = true;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        limitedeintentos = 3;
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 1.3"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                   
                    this.fallo();
                    if (limitedeintentos <= 0)
                    {
                        this.cargar("final");
                    }
                    else
                    {
                        StartCoroutine(cargarconespera(0.5f, "fallo"));
                        StartCoroutine(cargarconespera(4.3f, "puzzle nivel 1.2"));
                    }
                }
            }
			}
	}
     public void ordenNivel13(GameObject posiciones,GameObject piezas)
    {
			if (piezas.transform.GetChild(0).GetComponent<drap>().ubicado || piezas.transform.GetChild(1).GetComponent<drap>().ubicado || piezas.transform.GetChild(2).GetComponent<drap>().ubicado)
			{
				if(posiciones.transform.GetChild(0).GetComponent<Detector>().pieza.name=="45")
                {
                    if(!gano){
                        gano = true;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        limitedeintentos = 3;
                        StartCoroutine(cargarconespera(4.3f,"nivel 2"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    this.fallo();
                    if (limitedeintentos <= 0)
                    {
                        this.cargar("final");
                    }
                    else
                    {
                        StartCoroutine(cargarconespera(0.5f, "fallo"));
                        StartCoroutine(cargarconespera(4.3f, "puzzle nivel 1.3"));
                    }
                }
            }
			}
	}
    public void ordenNivel2(GameObject posiciones,GameObject piezas)
    {
			if (piezas.transform.GetChild(0).GetComponent<drap>().ubicado && piezas.transform.GetChild(1).GetComponent<drap>().ubicado && piezas.transform.GetChild(2).GetComponent<drap>().ubicado)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela")
                {
                    if(!gano){
                        gano = true;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        limitedeintentos = 3;
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 2.1"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                 
                    this.fallo();
                    if (limitedeintentos <= 0)
                    {
                        this.cargar("final");
                    }
                    else
                    {
                        StartCoroutine(cargarconespera(0.5f, "fallo"));
                        StartCoroutine(cargarconespera(4.3f, "puzzle nivel 2"));
                    }
                }
            }
			}
	}
    public void ordenNivel21(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==3)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo roja" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela verde")
                {
                    if(!gano){
                        gano = true;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        limitedeintentos = 3;
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 2.2"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;

                    this.fallo();
                    if (limitedeintentos <= 0)
                    {
                        this.cargar("final");
                    }
                    else
                    {
                        StartCoroutine(cargarconespera(0.5f, "fallo"));
                        StartCoroutine(cargarconespera(4.3f, "puzzle nivel 2.1"));
                    }
                }
            }
			}
        contador = 0;
    }
    public void ordenNivel22(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==4)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca amarilla" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela"&& posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca")
                {
                    if(!gano){
                        gano = true;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        limitedeintentos = 3;
                        StartCoroutine(cargarconespera(4.3f,"nivel 3"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    this.fallo();
                    if (limitedeintentos <= 0)
                    {
                        this.cargar("final");
                    }
                    else
                    {
                        StartCoroutine(cargarconespera(0.5f, "fallo"));
                        StartCoroutine(cargarconespera(4.3f, "puzzle nivel 2.2"));
                    }
                }
            }
			}
        contador = 0;
    }
    public void ordenNivel3(GameObject posiciones,GameObject piezas)
    {       
        tercernivel=true;
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo amarillo" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca morada" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela naranja" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca verde" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela roja")
                {
                    if(!gano){
                        gano = true;
                        this.sumarpuntuacion();
                        nivel3++;
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.1"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.1"));
                }
            }
			}
        contador = 0;
    }
    public void ordenNivel31(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo rojo" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca naranja" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela amarilla" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca morada" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela azul" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela 2 verde")
                {
                    if(!gano){
                        gano = true;
                        nivel3++;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.2"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.2"));
                }
            }
			}
        contador = 0;
    }
        public void ordenNivel32(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo azul" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca verde" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela amarilla" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca azul" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela morada" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela 2 roja")
                {
                    if(!gano){
                        gano = true;
                        nivel3++;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.3"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.3"));
                }
            }
			}
        contador = 0;
    }
    public void ordenNivel33(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo naranja" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela 2 verde" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca plateada" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela morada" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela 2 negra" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca amarilla")
                {
                    if(!gano){
                        gano = true;
                        nivel3++;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.4"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.4"));
                    // for (int i = 0; i < posiciones.transform.childCount; i++)
                    // {
                    //     Debug.Log(posiciones.transform.GetChild(i).transform.GetChild(0).GetComponent<Detector>().pieza.name);
                    // }
                }
            }
			}
        contador = 0;
    }
    public void ordenNivel34(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo morado" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela roja" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca verde" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela 2 morada" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca amarilla" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca naranja")
                {
                    if(!gano){
                        gano = true;
                        nivel3++;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.6"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.6"));
                }
            }
			}
        contador = 0;
    }
    public void ordenNivel36(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo amarillo" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca morada" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela naranja" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca verde" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela roja")
                {
                    if(!gano){
                        gano = true;
                        this.sumarpuntuacion();
                        nivel3++;
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.7"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.7"));
                }
            }
			}
        contador = 0;
    }
    public void ordenNivel37(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo rojo" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca naranja" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela amarilla" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca morada" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela azul" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela 2 verde")
                {
                    if(!gano){
                        gano = true;
                        nivel3++;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.8"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.8"));
                }
            }
			}
        contador = 0;
    }
     public void ordenNivel38(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo azul" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca verde" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela amarilla" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca azul" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela morada" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela 2 roja")
                {
                    if(!gano){
                        gano = true;
                        nivel3++;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.9"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.9"));
                }
            }
			}
        contador = 0;
    }
    public void ordenNivel39(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo morado" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela roja" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca verde" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela 2 morada" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca amarilla" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca naranja")
                {
                    if(!gano){
                        gano = true;
                        nivel3++;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.10"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.10"));
                }
            }
			}
        contador = 0;
    }
    public void ordenNivel310(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo naranja" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela 2 verde" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca plateada" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela morada" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela 2 negra" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca amarilla")
                {
                    if(!gano){
                        gano = true;
                        nivel3++;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.11"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.11"));
                    // for (int i = 0; i < posiciones.transform.childCount; i++)
                    // {
                    //     Debug.Log(posiciones.transform.GetChild(i).transform.GetChild(0).GetComponent<Detector>().pieza.name);
                    // }
                }
            }
			}
        contador = 0;
    }
    public void ordenNivel311(GameObject posiciones,GameObject piezas)
    {       
            for (int i = 0; i < piezas.transform.childCount; i++)
            {
                if(piezas.transform.GetChild(i).GetComponent<drap>().ubicado){
                contador++;
                }
            }
			if (contador==6)
			{
				if(posiciones.transform.GetChild(0).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tornillo amarillo" && posiciones.transform.GetChild(1).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca morada" && posiciones.transform.GetChild(2).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela naranja" && posiciones.transform.GetChild(3).transform.GetChild(0).GetComponent<Detector>().pieza.name=="tuerca verde" && posiciones.transform.GetChild(4).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela" && posiciones.transform.GetChild(5).transform.GetChild(0).GetComponent<Detector>().pieza.name=="arandela roja")
                {
                    if(!gano){
                        gano = true;
                        this.sumarpuntuacion();
                        nivel3++;
                        tercernivel=false;
                        StartCoroutine(cargarconespera(0.5f,"score"));
                        StartCoroutine(cargarconespera(4.3f, "mensaje final"));
                        StartCoroutine(cargarconespera(4.3f,"puzzle nivel 3.5"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    StartCoroutine(cargarconespera(0.5f, "fallo 2"));
                    StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.5"));
                }
            }
			}
        contador = 0;
    }

    public void ordenNivel35(GameObject posiciones,GameObject piezas)
    {
        float preResultado=nivel3/11f*100;
        int respuesta= (int)preResultado;
        if (piezas.transform.GetChild(0).GetComponent<drap>().ubicado || piezas.transform.GetChild(1).GetComponent<drap>().ubicado || piezas.transform.GetChild(2).GetComponent<drap>().ubicado || piezas.transform.GetChild(3).GetComponent<drap>().ubicado || piezas.transform.GetChild(4).GetComponent<drap>().ubicado || piezas.transform.GetChild(5).GetComponent<drap>().ubicado || piezas.transform.GetChild(6).GetComponent<drap>().ubicado || piezas.transform.GetChild(7).GetComponent<drap>().ubicado || piezas.transform.GetChild(8).GetComponent<drap>().ubicado || piezas.transform.GetChild(9).GetComponent<drap>().ubicado || piezas.transform.GetChild(10).GetComponent<drap>().ubicado || piezas.transform.GetChild(11).GetComponent<drap>().ubicado)
			{
				if(posiciones.transform.GetChild(0).GetComponent<Detector>().pieza.name==respuesta.ToString())
                {
                    if(!gano){
                        gano = true;
                        this.sumarpuntuacion();
                        StartCoroutine(cargarconespera(0.5f,"final"));
                    }                    
			    }
			    else
                {
                if (!perdio)
                {
                    perdio = true;
                    this.fallo();
                    if (limitedeintentos <= 0)
                    {
                        this.cargar("final");
                    }
                    else
                    {
                        StartCoroutine(cargarconespera(0.5f, "fallo"));
                        StartCoroutine(cargarconespera(4.3f, "puzzle nivel 3.5"));
                    }
                }
            }
			}
	}
    public void sumarpuntuacion(){
        Debug.Log("se ejecuta" + limitedeintentos);
        switch (limitedeintentos)
        {
            case 3:
                score += 100;
            break;
            case 2:
                score += 50;
            break;
            case 1:
                score += 25;
            break;
        }
        audio.clip = ganar;
        audio.Play();
    }

    public void fallo(){
        limitedeintentos--;
        audio.clip = perder;
        audio.Play();
    }
    IEnumerator cargarconespera(float tiempoespera,string escena)
	{
		
		yield return new WaitForSeconds(tiempoespera);
		this.cargar(escena);
        perdio = false;
        gano= false;
    }

    
}
