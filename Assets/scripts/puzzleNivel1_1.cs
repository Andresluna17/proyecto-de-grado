﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puzzleNivel1_1 : MonoBehaviour
{
    private GameObject manager,posiciones;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindWithTag("Manager");
        posiciones = GameObject.Find("posicion");
    }

    // Update is called once per frame
    void Update()
    {
        manager.GetComponent<GameManager>().ordenNivel11(posiciones,gameObject);
    }
}
