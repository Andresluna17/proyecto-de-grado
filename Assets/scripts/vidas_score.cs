﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vidas_score : MonoBehaviour
{   
    public GameObject manager;    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindWithTag("Manager");
        transform.GetChild(4).GetComponent<UnityEngine.UI.Text>().text=manager.GetComponent<GameManager>().score.ToString();
        transform.GetChild(3).GetComponent<UnityEngine.UI.Text>().text=manager.GetComponent<GameManager>().limitedeintentos.ToString();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
