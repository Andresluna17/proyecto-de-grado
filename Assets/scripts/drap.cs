﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drap : MonoBehaviour {
	public bool posi = false, ubicado = false,tamaño =true;
	Vector3 posicion, scala;
	float x, y;


	// Use this for initialization
	void Start() {
		 
		scala = transform.localScale;
		if (tamaño) {
		x = scala[0] / 2;
		y = scala[1] / 2;
		transform.localScale = new Vector3(x, y, -1f);
		}
	}

	void Update() {
		// Update is called once per frame

	}

	void OnMouseDrag() {
		Vector3 posmouse = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
		transform.position = new Vector3(posmouse.x, posmouse.y, -1f);
		transform.localScale = scala;
	}

	void OnMouseUp() {
		if (posi)
		{
			transform.localScale = scala;
			transform.position = posicion;
			ubicado = true;
		} else {
            if (tamaño)
            {
                transform.localScale = new Vector3(x, y, -1f);
            }
            ubicado = false;
		}
	}

	void OnTriggerStay2D(Collider2D objeto) {
		if (objeto.gameObject.tag == "detector") {
			posi = true;
			posicion = objeto.gameObject.transform.position;
			//Debug.Log (objeto);
		}
	}
	void OnTriggerExit2D(Collider2D objeto) {
		if (objeto.gameObject.tag == "detector") {
			posi = false;
			//Debug.Log (objeto);
		}
	}
}
