﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tiempo : MonoBehaviour
{   
    public GameObject manager;    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindWithTag("Manager");
        Debug.Log(manager.GetComponent<GameManager>().tiempo.ToString());
        string pieza=manager.GetComponent<GameManager>().nivel3.ToString()+"/11";
        transform.GetChild(3).GetComponent<UnityEngine.UI.Text>().text=pieza;
    }

    // Update is called once per frame
    void Update()
    {
        float tiempo=manager.GetComponent<GameManager>().tiempo;
        int seconds = (int)tiempo % 60;
        int minutes = (int)tiempo / 60;
        string time = minutes + ":" + seconds;
        Debug.Log(time);
        transform.GetChild(4).GetComponent<UnityEngine.UI.Text>().text=time;
    }
}
